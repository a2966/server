# Server

Repository for api-doc server

This will be used by the users as a package. The server will then generate a json file from the user's codebase

For adding a route to the docs, use the following syntax above the route controller

```js
	/**
	 * @route
	 * @name
	 * @headers
	 * @body
	 * */
	const controller = (req,res) => {...}
```
