const express = require("express");
const cors = require("cors");
const { getTimestamp } = require("./utils/date-utils");

const app = express();
app.use(cors());
app.use(express.json());

console.log(getTimestamp(new Date(), "YYYY-MM-DD HHMMSS"));

const PORT = process.env.PORT || 5000;

app.get("/", (req, res) => {
  console.log("Welcome to api-doc server");
});

app.listen(PORT, console.log(`Server Running on Port : ${PORT}`));
