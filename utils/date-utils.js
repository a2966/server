exports.getTimestamp = (date, format) => {
  let day = date.getDate();

  let mnth = date.getMonth() + 1;
  let yr = date.getFullYear();
  let hh = date.getHours();

  let mm = date.getMinutes();

  let ss = date.getSeconds();

  if (format === "YYYY-MM-DD HHMMSS")
    return `${yr}-${mnth}-${day} ${hh}${mm}${ss}`;
  if (format === "MM/DD/YYYY HH:MM:SS")
    return `${mnth}/${day}/${yr} ${hh}:${mm}:${ss}`;
};
